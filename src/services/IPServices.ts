/*
GET https://geo.ipify.org/api/v2/country,city?apiKey=at_XodJ5Mos8SFz5xmPoZncGqtnufOxC&ipAddress=8.8.8.8&ipAddress=

output format
{
    "ip": "8.8.8.8",
    "location": {
        "country": "US",
        "region": "California",
        "city": "Mountain View",
        "lat": 37.40599,
        "lng": -122.078514,
        "postalCode": "94043",
        "timezone": "-07:00",
        "geonameId": 5375481
    },
    "domains": [
        "0d2.net",
        "003725.com",
        "0f6.b0094c.cn",
        "007515.com",
        "0guhi.jocose.cn"
    ],
    "as": {
        "asn": 15169,
        "name": "Google LLC",
        "route": "8.8.8.0/24",
        "domain": "https://about.google/intl/en/",
        "type": "Content"
    },
    "isp": "Google LLC"
}
*/

import { map, Observable } from "rxjs";
import { TrackInfo } from "../model/entities";
import Axios from 'axios-observable';


class IPServices {

  private static _instance: IPServices;
  private static readonly proxyUrl = 'https://cors-anywhere.herokuapp.com/';
  private static readonly ipifyUrl = 'https://geo.ipify.org/api/v2/country,city';

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  newTrackInfo = (): TrackInfo => {
    return {
      ip: "-",
      location: { country: "", region: "Region", city: "City", lat: 0, lng: 0, postalCode: "", timezone: "00:00", geonameId: 0 },
      isp: "-"
    } as TrackInfo
  };

  dummyApi(ip: string): Observable<TrackInfo> {
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.newTrackInfo())
      }, 1000);
    });
  }

  getTrackInfo(ip: string): Observable<TrackInfo> {
    return Axios.get<TrackInfo>(IPServices.proxyUrl + IPServices.ipifyUrl,
      {
        params: { apiKey: import.meta.env.VITE_IPIFY_KEY, ipAddress: ip }
      })
      .pipe(
        map(({ data }) => data)
      );
  }
}

export const _IPServices = IPServices.Instance;
