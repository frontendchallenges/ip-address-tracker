import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ command }) => ({
  plugins: [vue()],
  base: command === 'build' ? "/ip-address-tracker/" : "",
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "./src/css/tokens.scss";
        `
      }
    }
  }
}));
