# Frontend Mentor - Interactive rating card component solution

This is a solution to the [Interactive rating card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/interactive-rating-component-koxpeBUmI). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements
- Build out this IP Address Tracker app and get it looking as close to the design as possible. To get the IP Address locations, using the IP Geolocation API by IPify|https://geo.ipify.org/. To generate the map, I use LeafletJS|https://leafletjs.com/.


This is an intermediate project to:
* practice fundamental concepts
* Deploy to Gitlab pages

### Links

- Live Site URL: [Live Solution](https://frontendchallenges.gitlab.io/ip-address-tracker/) Unfortunately because there's a private api-key (.env.local file) this is included in .gitignore and this way the solution won't work except locally.
- Screenshot: [IP Tracker](./screenshot.png)

## My process

1. Create project `yarn vite create`
   Add dependencies `yarn add --dev sass`
   Edit vite.config.ts to set the base publish URL
   ```base: command === 'build' ? "/ip-address-tracker/" : "",```
   Also to add tokens.scss to the global css
2. Create tokens.scss with the constant values for colors, fonts, and sizes, and styles.scss for general styling
  The styles.scss must be included in main.js and tokens.scss in vite.config.ts
3. Set a backgroud color for body
4. Think of the design by splitting the layout in Components:
  App    
    IPAddressTracker (main)
      *Background
      Title
      IP Input + icon
      Card
        div.group (IP)
          span.label 
          span.value
        div.group (Location)
          span.label 
          span.value
        div.group (Timezone)
          span.label 
          span.value
        div.group (ISP)
          span.label 
          span.value
      Map
    Attribution
5. Create a card center vertically and horizontally
6. Think about the desktop design. Use a flex display for the card and when it's in desktop, change flex-direction to row
7. Notice and implement the active states.
8. Implement logic: get the IP Address locations and generate the map.
9. Deploy to gitlab pages (remember configure the base, e.g. vite build --base=./)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Vue + Vite
- IPify
- Leaflet

### Lessons

To set a border in all the InfoGroup elements except the first one.

```css
.group:not(:first-child) {
    border-left: 1px solid $color-label;
  }  
```

To show a loadign message according to the loading variable
```html
 <p class="loader" v-if="loading">Loading...</p>
```
 To use an environmental variable VITE_IPIFY_KEY defined in the .env file in vite

```js
import.meta.env.VITE_IPIFY_KEY
```

Create a class and use the singleton pattern
```js
class IPServices {
  private static _instance: IPServices;

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }
```

Using Axios with observables
```js
observable = Axios.get<TrackInfo>("https://geo.ipify.org/api/v2/country,city",
      {
        params: { apiKey: 'apikey', ipAddress: ip }
      })
      .pipe(
        map(({ data }) => data)
      );
//and then use the Observable<TrackInfo> result
observable.subscribe({
    next: (data) => {
      trackInfo.value = data;
      drawMap(trackInfo.value.location.lat, trackInfo.value.location.lng);
      loading.value = false;
    },
    error: (err) => {
      console.log(err.response);
    }
```

### Continued development

This is one of many projects, I plan to build to make more experience with FrontEnd development, specially with the Framework, handling spacing and practicing css.


### Useful resources

- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)
- [IPify](https://geo.ipify.org/)
- [LeafletJS](https://leafletjs.com/)

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.